var exec = require('cordova/exec');

var backgroundGeoLocation = {
    /**
     * @property {Object} stationaryRegion
     */
    stationaryRegion: null,

    /**
     * @property {Object} service
     */
    service: {
        ANDROID_DISTANCE_FILTER: 0,
        ANDROID_FUSED_LOCATION: 1
    },

    accuracy: {
        HIGH: 0,
        MEDIUM: 100,
        LOW: 1000,
        PASSIVE: 10000
    },

    /**
     * @property {Object} config
     */
    config: {},

    configure: function(success, failure, config) {
        this.config = config || {};
        var desiredAccuracy       = (config.desiredAccuracy >= 0) ? config.desiredAccuracy  : this.accuracy.MEDIUM,
            activityType          = config.activityType || 'OTHER',
            notificationTitle     = config.notificationTitle || 'Background tracking',
            notificationText      = config.notificationText || 'ENABLED',
            notificationIcon      = config.notificationIcon,
            notificationIconColor = config.notificationIconColor,
            interval              = (config.interval >= 0) ? config.interval : locationTimeout * 1000, // milliseconds
            fastestInterval       = (config.fastestInterval >= 0) ? config.fastestInterval  : 120000; // milliseconds

        exec(success || function() {},
            failure || function() {},
            'BackgroundGeoLocation',
            'configure', [
                desiredAccuracy,
                activityType,
                notificationTitle,
                notificationText,
                notificationIcon,
                notificationIconColor,
                interval,
                fastestInterval,
            ]
        );
    },
    start: function(success, failure) {
        exec(success || function() {},
            failure || function() {},
            'BackgroundGeoLocation',
            'start', []);
    },
    stop: function(success, failure) {
        exec(success || function() {},
            failure || function() {},
            'BackgroundGeoLocation',
            'stop', []);
    },
    finish: function(success, failure) {
        exec(success || function() {},
            failure || function() {},
            'BackgroundGeoLocation',
            'finish', []);
    },
    isLocationEnabled: function(success, failure) {
        exec(success || function() {},
            failure || function() {},
            'BackgroundGeoLocation',
            'isLocationEnabled', []);
    },
    showLocationSettings: function() {
        exec(function() {},
            function() {},
            'BackgroundGeoLocation',
            'showLocationSettings', []);
    },
    watchLocationMode: function(success, failure) {
        if (typeof(success) !== 'function') {
             throw 'BackgroundGeolocation#watchLocationMode requires a success callback';
        }
        exec(success,
            failure || function() {},
            'BackgroundGeoLocation',
            'watchLocationMode', []);
    },
    stopWatchingLocationMode: function() {
        exec(function() {},
            function() {},
            'BackgroundGeoLocation',
            'stopWatchingLocationMode', []);
    },
    getLocations: function(success, failure) {
        if (typeof(success) !== 'function') {
             throw 'BackgroundGeolocation#getLocations requires a success callback';
        }
        exec(success,
            failure || function() {},
            'BackgroundGeoLocation',
            'getLocations', []);
    },
    deleteLocation: function(locationId, success, failure) {
        exec(success || function() {},
            failure || function() {},
            'BackgroundGeoLocation',
            'deleteLocation', [locationId]);
    },
    deleteAllLocations: function(success, failure) {
        exec(success || function() {},
            failure || function() {},
            'BackgroundGeoLocation',
            'deleteAllLocations', []);
    }
};

/* @Deprecated */
window.plugins = window.plugins || {};
window.plugins.backgroundGeoLocation = backgroundGeoLocation;

module.exports = backgroundGeoLocation;

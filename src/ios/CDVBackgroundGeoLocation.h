#import <Cordova/CDVPlugin.h>
#import <CoreLocation/CoreLocation.h>

@interface CDVBackgroundGeoLocation : CDVPlugin <CLLocationManagerDelegate>

@property (nonatomic, strong) NSString* syncCallbackId;

- (void) configure:(CDVInvokedUrlCommand*)command;
- (void) start:(CDVInvokedUrlCommand*)command;
- (void) stop:(CDVInvokedUrlCommand*)command;
- (void) finish:(CDVInvokedUrlCommand*)command;
- (void) onAppTerminate;

@end


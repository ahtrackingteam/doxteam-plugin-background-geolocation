package com.doxteam;

/**
 * Constants
 */
public abstract class Constant
{
    public static final String DATA = "DATA";
    public static final String ACTION = "ACTION";
    public static final String ACTION_FILTER = "com.tenforwardconsulting.cordova.bgloc.ACTION";
    public static final int ACTION_LOCATION_UPDATE = 0;
    public static final int ACTION_ACTIVITY_KILLED = 3;
}

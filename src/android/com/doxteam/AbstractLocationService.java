package com.doxteam;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import org.json.JSONException;

import java.util.Collection;
import java.util.Random;

public abstract class AbstractLocationService extends Service {
    private static final String TAG = "AbstractLocationService";

    protected Config config;
    protected String activity;

    protected Location lastLocation;
    protected Boolean mainActivityDestroyed = false;

    private BroadcastReceiver actionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle data = intent.getExtras();
            switch (data.getInt(Constant.ACTION)) {
                case Constant.ACTION_ACTIVITY_KILLED:
                    Log.w(TAG, "Main activity was killed! Start persisting locations from now.");
                    mainActivityDestroyed = data.getBoolean(Constant.DATA);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        Log.i(TAG, "OnBind" + intent);
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerReceiver(actionReceiver, new IntentFilter(Constant.ACTION_FILTER));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);
        if (intent != null) {
            config = intent.getParcelableExtra("config");
            activity = intent.getStringExtra("activity");
            Log.d(TAG, "Got activity" + activity);

            // Build a Notification required for running service in foreground.
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setContentTitle(config.getNotificationTitle());
            builder.setContentText(config.getNotificationText());
            builder.setSmallIcon(android.R.drawable.ic_menu_mylocation);
            if (config.getNotificationIcon() != null) {
                builder.setSmallIcon(getPluginResource(config.getSmallNotificationIcon()));
                builder.setLargeIcon(BitmapFactory.decodeResource(getApplication().getResources(), getPluginResource(config.getLargeNotificationIcon())));
            }
            if (config.getNotificationIconColor() != null) {
                builder.setColor(this.parseNotificationIconColor(config.getNotificationIconColor()));
            }

            setClickEvent(builder);

            Notification notification = builder.build();
            notification.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_FOREGROUND_SERVICE | Notification.FLAG_NO_CLEAR;
            startForeground(startId, notification);
        }
        Log.i(TAG, config.toString());
        Log.i(TAG, "- activity: " + activity);

        mainActivityDestroyed = false;
        Collection<LocationProxy> locations = getAllLocations();
        deleteAllLocations();
        for (LocationProxy location : locations) {
            handleProxyLocation(location);
        }

        //We want this service to continue running until it is explicitly stopped
        return START_STICKY;
    }

    public Integer getPluginResource(String resourceName) {
        return getApplication().getResources().getIdentifier(resourceName, "drawable", getApplication().getPackageName());
    }

    /**
     * Adds an onclick handler to the notification
     */
    protected NotificationCompat.Builder setClickEvent(NotificationCompat.Builder builder) {
        int requestCode = new Random().nextInt();
        Context context = getApplicationContext();
        String packageName = context.getPackageName();
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context, requestCode, launchIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        return builder.setContentIntent(contentIntent);
    }

    private Integer parseNotificationIconColor(String color) {
        int iconColor = 0;
        if (color != null) {
            try {
                iconColor = Color.parseColor(color);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "couldn't parse color from android options");
            }
        }
        return iconColor;
    }

    @Override
    public boolean stopService(Intent intent) {
        Log.i(TAG, "- Received stop: " + intent);
        cleanUp();
        unregisterReceiver(actionReceiver);
        return super.stopService(intent); // not needed???
    }

    protected abstract void cleanUp();

    protected void broadcastLocation(LocationProxy location) {
        Log.d(TAG, "Broadcasting update message: " + location.toString());
        try {
            String locStr = location.toJSONObject().toString();
            Intent intent = new Intent(Constant.ACTION_FILTER);
            intent.putExtra(Constant.ACTION, Constant.ACTION_LOCATION_UPDATE);
            intent.putExtra(Constant.DATA, locStr);
            this.sendBroadcast(intent);
        } catch (JSONException e) {
            Log.w(TAG, "Failed to broadcast location");
        }
    }

    protected void persistLocation(LocationProxy location) {
        LocationDAO dao = DAOFactory.createLocationDAO(this.getApplicationContext());

        if (dao.persistLocation(location)) {
            Log.d(TAG, "Persisted Location: " + location);
        } else {
            Log.w(TAG, "Failed to persist location");
        }
    }

    protected void handleProxyLocation(LocationProxy location) {
        if (mainActivityDestroyed) {
            Log.d(TAG, "Persisting location. Reason: Main activity was killed.");
            persistLocation(location);
        }
        broadcastLocation(location);
    }

    protected void handleLocation(Location location) {
        LocationProxy bgLocation = LocationProxy.fromAndroidLocation(location);
        handleProxyLocation(bgLocation);
    }

    @Override
    public void onDestroy() {
        Log.w(TAG, "------------------------------------------ Destroyed Location update Service");
        cleanUp();
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        this.stopSelf();
        super.onTaskRemoved(rootIntent);
    }


    public Collection<LocationProxy> getAllLocations() {
        Context context = this.getApplicationContext();
        LocationDAO dao = DAOFactory.createLocationDAO(context);
        return dao.getAllLocations();
    }

    public void deleteAllLocations() {
        Context context = this.getApplicationContext();
        LocationDAO dao = DAOFactory.createLocationDAO(context);
        dao.deleteAllLocations();
    }

//    public JSONArray getAllLocations() throws JSONException {
//        JSONArray jsonLocationsArray = new JSONArray();
//        Context context = this.getApplicationContext();
//        LocationDAO dao = DAOFactory.createLocationDAO(context);
//        Collection<LocationProxy> locations = dao.getAllLocations();
//        for (LocationProxy location : locations) {
//            jsonLocationsArray.put(location.toJSONObject());
//        }
//        return jsonLocationsArray;
//    }

//    public void deleteLocation(Integer locationId) {
//        Context context = this.getApplicationContext();
//        LocationDAO dao = DAOFactory.createLocationDAO(context);
//        dao.deleteLocation(locationId);
//    }


}

package com.doxteam;

import org.json.JSONArray;
import org.json.JSONException;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Config class
 */
public class Config implements Parcelable
{
    private Integer desiredAccuracy = 100;
    private String notificationTitle = "Background tracking";
    private String notificationText = "ENABLED";
    private String activityType; //not used
    private String notificationIcon;
    private String notificationIconColor;
    private Integer interval = 900000; //milliseconds
    private Integer fastestInterval = 120000; //milliseconds

    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(getDesiredAccuracy());
        out.writeString(getNotificationTitle());
        out.writeString(getNotificationText());
        out.writeString(getActivityType());
        out.writeString(getNotificationIcon());
        out.writeString(getNotificationIconColor());
        out.writeInt(getInterval());
        out.writeInt(getFastestInterval());
    }

    public static final Parcelable.Creator<Config> CREATOR
            = new Parcelable.Creator<Config>() {
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    public Config () {

    }

    private Config(Parcel in) {
        setDesiredAccuracy(in.readInt());
        setNotificationTitle(in.readString());
        setNotificationText(in.readString());
        setActivityType(in.readString());
        setNotificationIcon(in.readString());
        setNotificationIconColor(in.readString());
        setInterval(in.readInt());
        setFastestInterval(in.readInt());
    }

    public Integer getDesiredAccuracy() {
        return desiredAccuracy;
    }

    public void setDesiredAccuracy(Integer desiredAccuracy) {
        this.desiredAccuracy = desiredAccuracy;
    }


    public String getNotificationIconColor() {
        return notificationIconColor;
    }

    public void setNotificationIconColor(String notificationIconColor) {
        if (!"null".equals(notificationIconColor)) {
            this.notificationIconColor = notificationIconColor;
        }
    }

    public String getNotificationIcon() {
        return notificationIcon;
    }

    public void setNotificationIcon(String notificationIcon) {
        if (!"null".equals(notificationIcon)) {
            this.notificationIcon = notificationIcon;
        }
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getFastestInterval() {
        return fastestInterval;
    }

    public void setFastestInterval(Integer fastestInterval) {
        this.fastestInterval = fastestInterval;
    }

    public String getLargeNotificationIcon () {
        String iconName = getNotificationIcon();
        if (iconName != null) {
            iconName = iconName + "_large";
        }
        return iconName;
    }

    public String getSmallNotificationIcon () {
        String iconName = getNotificationIcon();
        if (iconName != null) {
            iconName = iconName + "_small";
        }
        return iconName;
    }

    private void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    private String getActivityType() {
        return activityType;
    }

    @Override
    public String toString () {
        return new StringBuffer()
                .append("- desiredAccuracy: "       + getDesiredAccuracy())
                .append("- notificationIcon: "      + getNotificationIcon())
                .append("- notificationIconColor: " + getNotificationIconColor())
                .append("- notificationTitle: "     + getNotificationTitle())
                .append("- notificationText: "      + getNotificationText())
                .append("- interval: "              + getInterval())
                .append("- fastestInterval: "       + getFastestInterval())
                .toString();
    }

    public static Config fromJSONArray (JSONArray data) throws JSONException {
        Config config = new Config();
        config.setDesiredAccuracy(data.getInt(0));
        config.setActivityType(data.getString(1));
        config.setNotificationTitle(data.getString(2));
        config.setNotificationText(data.getString(3));
        config.setNotificationIcon(data.getString(4));
        config.setNotificationIconColor(data.getString(5));
        config.setInterval(data.getInt(6));
        config.setFastestInterval(data.getInt(7));

        return config;
    }
}

package com.doxteam;

import java.util.Collection;

public interface LocationDAO {
    Collection<LocationProxy> getAllLocations();
    boolean persistLocation(LocationProxy l);
    void deleteLocation(Integer locationId);
    void deleteAllLocations();
}
